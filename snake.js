const canvas = document.getElementById("snake");
const ctx = canvas.getContext("2d");

// create the unit
const box = 32;

// IMAGES

const ground = new Image();
ground.src = "img/ground.png";

const foodImg = new Image();
foodImg.src = "img/apple.png";

// SONS

let dead = new Audio();
let eat = new Audio();
// let up = new Audio();
// let right = new Audio();
// let left = new Audio();
// let down = new Audio();

dead.src = "audio/dead.mp3";
eat.src = "audio/eating.mp3";
// Sound effect pour chaque mouvement
// up.src = "audio/up.mp3";
// right.src = "audio/right.mp3";
// left.src = "audio/left.mp3";
// down.src = "audio/down.mp3";

// SERPENT

let snake = [];

snake[0] = {
    x : 9 * box,
    y : 10 * box
};

// POMME

let food = {
    x : Math.floor(Math.random()*17+1) * box,
    y : Math.floor(Math.random()*15+3) * box
};

// Le Score commence a 0

let score = 0;

// Evenements de control du Serpent

let d; //d represente la direction

document.addEventListener("keydown",direction);

function direction(event){
    let key = event.keyCode;
    if( key === 37 && d !== "RIGHT"){
        // left.play();
        d = "LEFT";
    }else if(key === 38 && d !== "DOWN"){
        d = "UP";
        // up.play();
    }else if(key === 39 && d !== "LEFT"){
        d = "RIGHT";
        // right.play();
    }else if(key === 40 && d !== "UP"){
        d = "DOWN";
        // down.play();
    }
}

// fonction de collision
function collision(head,array){
    for(let i = 0; i < array.length; i++){
        if(head.x === array[i].x && head.y === array[i].y){
            return true;
        }
    }
    return false;
}

// function de dessin

function draw(){

    ctx.drawImage(ground,0,0);

    for( let i = 0; i < snake.length ; i++){
        ctx.fillStyle = ( i === 0 )? "#108352" : "#12935c";
        ctx.fillRect(snake[i].x,snake[i].y,box,box);

        ctx.strokeStyle = "#108352";
        ctx.strokeRect(snake[i].x,snake[i].y,box,box);
    }

    ctx.drawImage(foodImg, food.x, food.y);

    //  position de la tete
    let snakeX = snake[0].x;
    let snakeY = snake[0].y;

    // definie la direction
    if( d === "LEFT") snakeX -= box;
    if( d === "UP") snakeY -= box;
    if( d === "RIGHT") snakeX += box;
    if( d === "DOWN") snakeY += box;

    // quand le serpent mange la pomme
    if(snakeX === food.x && snakeY === food.y){
        score++;
        eat.play();
        food = {
            x : Math.floor(Math.random()*17+1) * box,
            y : Math.floor(Math.random()*15+3) * box
        }
        // enleve pas la queue
    }else{
        // enleve la queue
        snake.pop();
    }

    // ajoute une nouvelle tete

    let newHead = {
        x : snakeX,
        y : snakeY
    };

    // condition de collision

    if(snakeX < box || snakeX > 17 * box || snakeY < 3*box || snakeY > 17*box || collision(newHead,snake)){
        clearInterval(game);
        dead.play().then();
    }

    snake.unshift(newHead);

    ctx.fillStyle = "white";
    ctx.font = "45px Changa one";
    ctx.fillText(`${score}`,2*box,1.6*box);
}

// appel la fonction 'draw' toute les 100ms

let game = setInterval(draw,100);


















